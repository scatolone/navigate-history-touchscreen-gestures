(async () => {

	let minTravelDistance = 200;
	let isScrolling = false;
	let fingers = [];
	let maxFingers = 1;
	let singleGestureFingerNum = 0;
	let objectsMoving = new Set();

	document.addEventListener('touchstart', e => {
		singleGestureFingerNum++;
		fingers.push({
			identifier: e.changedTouches[0].identifier,
			startX: e.changedTouches[0].screenX,
			target: e.target
		});
	})

	document.addEventListener('touchend', e => {
		let finger = fingers.find(f => f.identifier == e.changedTouches[0].identifier);
		if (finger) {
			finger.endX = e.changedTouches[0].screenX;
			navigateAndPop(finger);
		}
	})

	window.addEventListener('scroll', e => {
		var el = e.target;
		while (el && el !== document && el !== window && !isScrollable(el)) {
			el = el.parent;
		}
		if (el.scrollLeft > 0) {
			isScrolling = true;
		}
	}, true);

	window.addEventListener('scrollend', e => {
		isScrolling = false;
	}, true);

	addMutationObserverForMovingObjects();

	function isScrollable(el) {
		return el.scrollWidth > el.clientWidth || el.scrollHeight > el.clientHeight;
	}

	function navigateAndPop(leavingFinger) {

		if (singleGestureFingerNum == maxFingers && !isScrolling && !isZooming() && !isMovingAnObject(leavingFinger.target)) {

			let endX = leavingFinger.endX;

			if (fingers.every(f => endX + minTravelDistance < f.startX)) {
				history.back();
			}

			if (fingers.every(f => endX - minTravelDistance > f.startX)) {
				history.forward();
			}

		}

		fingers = fingers.filter(f => f.identifier != leavingFinger.identifier);
		if (fingers.length == 0) {
			singleGestureFingerNum = 0;
			objectsMoving.clear();
		}

	}

	function isZooming() {
		return window.visualViewport.scale != 1;
	}

	function isMovingAnObject(touchedElement) {
		return [...objectsMoving].some(obj => obj === touchedElement || obj.contains(touchedElement));
	}

	function addMutationObserverForMovingObjects() {

		const config = { attributes: true, childList: true, subtree: true };

		const callback = (mutationList, observer) => {
			for (const mutation of mutationList) {
				console.log(objectsMoving.size);
				if (mutation.type === "attributes" && mutation.attributeName == "style" && singleGestureFingerNum > 0) {
					objectsMoving.add(mutation.target);
				}
			}
		};

		const observer = new MutationObserver(callback);
		observer.observe(document, config);

	}


})();